#pragma once

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <stdexcept>
#include <functional>
#include <iostream>


namespace Eq {

namespace Private {

class SpecMapPrivate;
class FuncProcessorPrivate;

} // namespace Private

using SpecParam = std::pair<std::string, std::string>;
using SpecParams = std::vector<SpecParam>;
using CallParams = std::vector<double>;

struct FunctionSpec {
	std::string name;
	SpecParams params;
};

class Parser {
public:
	Parser() = default;
	virtual ~Parser() = default;

	virtual FunctionSpec parseSpec(const std::string &f) = 0;
};


class SpecMap {
public:
	using ValueType = std::map<std::string, FunctionSpec>;

	static SpecMap &inst();

	SpecMap(const SpecMap &) = delete;
	SpecMap(const SpecMap &&) = delete;
	SpecMap& operator=(const SpecMap &) = delete;
	SpecMap& operator=(const SpecMap &&) = delete;

	const ValueType &all() const;
	void reg(const std::string &);
	const FunctionSpec &get(const std::string &key) const;
private:
	friend class SpecMapPrivate;
	friend std::ostream &operator<<(std::ostream &ostr, const SpecMap &sm);
	std::unique_ptr<Private::SpecMapPrivate> d;
private:
	SpecMap();
	~SpecMap();
};


class FuncProcessor {
public:
	FuncProcessor();
	~FuncProcessor();
	double process(const std::string &func);
private:
	friend class CalculatorPrivate;
	std::unique_ptr<Private::FuncProcessorPrivate> d;
};


std::ostream &operator<<(std::ostream &ostr, const SpecParam &sp);
std::ostream &operator<<(std::ostream &ostr, const SpecParams &sps);
std::ostream &operator<<(std::ostream &ostr, const FunctionSpec &fs);
std::ostream &operator<<(std::ostream &ostr, const SpecMap &sm);


#define __DO_DECLARE_EXCEPTION__(class_, ext_, base_) \
  class class_##ext_ \
    : public base_ \
  { \
  public: \
    template <typename ... Args> \
    class_##ext_(Args const & ... args) \
      : base_(args ... ) \
    { \
    } \
  };

#define DO_DECLARE_RUNTIME_BASE_PARSE_ERROR(class_) \
  __DO_DECLARE_EXCEPTION__(class_, ParseError, std::runtime_error)

DO_DECLARE_RUNTIME_BASE_PARSE_ERROR(Base);

#define DO_DECLARE_RUNTIME_PARSE_ERROR(class_) \
  __DO_DECLARE_EXCEPTION__(class_, ParseError, BaseParseError)

#define DO_DECLARE_RUNTIME_ERROR(class_) \
  __DO_DECLARE_EXCEPTION__(class_, Error, std::runtime_error)

/// Exception declarations
DO_DECLARE_RUNTIME_PARSE_ERROR(WholeString);
DO_DECLARE_RUNTIME_PARSE_ERROR(FuncName);
DO_DECLARE_RUNTIME_PARSE_ERROR(ArgumentsString);
DO_DECLARE_RUNTIME_PARSE_ERROR(SinleArgumentString);
DO_DECLARE_RUNTIME_PARSE_ERROR(InvalidSyntax);
DO_DECLARE_RUNTIME_PARSE_ERROR(SpecMismatch);
DO_DECLARE_RUNTIME_PARSE_ERROR(Validation);

DO_DECLARE_RUNTIME_ERROR(NotRegisteredSpec);
DO_DECLARE_RUNTIME_ERROR(SpecExisting);
DO_DECLARE_RUNTIME_ERROR(InputData);

} // namespace Eq

