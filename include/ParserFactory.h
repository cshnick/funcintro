#pragma once

#include "Common.h"

namespace Eq {

namespace Name {

extern std::string RDPParser;
extern std::string BoostRegexParser;
extern std::string StateMachineParser;
extern std::string LexParser;

} // namespace Name

class AbstractParserCreator {
public:
	AbstractParserCreator() = default;
	virtual ~AbstractParserCreator() = default;

	virtual Parser *create() const = 0;
};

template<typename T>
class ParserCreator: public AbstractParserCreator {
public:
	virtual Parser *create() const {
		return new T();
	}
};

class ParserFactory {
public:
	ParserFactory(const ParserFactory &) = delete;
	ParserFactory(const ParserFactory &&) = delete;
	ParserFactory& operator=(const ParserFactory &) = delete;
	ParserFactory& operator=(const ParserFactory &&) = delete;

	static ParserFactory &inst();

	template<typename T>
	void reg(const std::string & id) {
		typename FactoryMap::iterator it = factory_.find(id);
		if (it == factory_.end())
			factory_[id] = new ParserCreator<T>();
	}
	Parser * create(const std::string & id) {
		typename FactoryMap::iterator it = factory_.find(id);
		if (it != factory_.end())
			return it->second->create();
		return 0;
	}

protected:
	using FactoryMap = std::map<std::string, AbstractParserCreator*>;
	FactoryMap factory_;

	ParserFactory();
};

} //namespace Eq
