#include "LexParser.h"

#include <deque>
#include <string>

#include <functional>
#include <algorithm>
#include <cctype>
#include <locale>
#include <iostream>
#include <locale>
#include <string>
#include <sstream>

namespace Eq {

enum class TokenType : int16_t {
	IDENTIFIER,
	KEYWORD,
	NUMBER,
//	REL_OP, 	// such as ==  <  >  =!=    =>  =<
	OP,			// such as = :  +  -  *  / %
	DELIM,		// such as . (  ) , { } ; [ ] 

	UNDEF,		// undefined
	EOT 		// end of token
} ;


class Token {
public:
	Token() :
		type_(TokenType::UNDEF)
	{}
	Token(TokenType type, const std::string& value = std::string()) :
		type_(type), value_(std::move(value)) {
		trim(value_);
	};
	virtual ~Token() = default;

	TokenType get_type() const { return type_; }
	std::string get_value() const { return value_; }
	bool is_valid(const std::string& value = std::string()) const{
		bool isValid(false);
		switch (type_) {
			case Eq::TokenType::KEYWORD: {
				if (!value_.empty()) {
					if (!std::isalpha(static_cast<unsigned char>(*value_.begin())))
						break;
					if (value_.length() != (size_t)std::count_if(value_.begin(), value_.end(),
						[](unsigned char c) { return std::isalnum(c); }))
						break;

					isValid = true;
				}
			} break;
			case Eq::TokenType::NUMBER: {
				std::istringstream s3(value_);
				auto& f = std::use_facet<std::num_get<char>>(s3.getloc());
				std::istreambuf_iterator<char> beg(s3), end;
				double f3;
				std::ios::iostate err = std::ios::goodbit;
				f.get(beg, end, s3, err, f3);
				if (err == std::ios::eofbit)
					isValid = true;
			} break;
			case Eq::TokenType::IDENTIFIER:
			case Eq::TokenType::OP:
			case Eq::TokenType::DELIM:
			case Eq::TokenType::UNDEF:
			case Eq::TokenType::EOT:
			default:
				isValid = true;
				break;
		}

		return isValid;
	}

private:
	void ltrim(std::string &s) const	{
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
			return !std::isspace(ch);
		}));
	}
	void rtrim(std::string &s) const {
		s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
			return !std::isspace(ch);
		}).base(), s.end());
	}
	void trim(std::string &s) const {
		ltrim(s);
		rtrim(s);
	}

private:
	TokenType type_;
	std::string value_;
};


class Tokens : public std::deque<Token> {

public:
	Tokens() {};
	virtual ~Tokens() = default;

	int accept(TokenType tt, std::string& value) {
		if (this->empty()) {
			return 0;
		}
		std::deque<Token>::const_iterator it(this->begin());
		if (it->get_type() == tt) {
			if (!it->is_valid())
			{
				throw ValidationParseError("Validation failed. Value : " + it->get_value());
			}
			value = it->get_value();
			this->pop_front();
			return 1;
		}
		return 0;
	}
	int expect(TokenType tt) {
		std::string value;
		if (accept(tt, value))
			return 1;
		else
			return 0;
	}
	void add(const Token& t) {
		this->emplace_back(std::move(t));
	}
};

void ParseArg(const std::string& arg, Tokens& tokens)
{
	std::string::size_type assign_pos = arg.find('=');
	if (assign_pos != std::string::npos) {
		tokens.add(Token(TokenType::IDENTIFIER, std::string(arg, 0, assign_pos)));
		tokens.add(Token(TokenType::OP));
		tokens.add(Token(TokenType::NUMBER, std::string(arg, ++assign_pos, arg.length() - assign_pos)));
		tokens.add(Token(TokenType::DELIM));
	} else 	if (!arg.empty()) {
		tokens.add(Token(TokenType::IDENTIFIER, arg));
		tokens.add(Token(TokenType::DELIM));
	} else {
		throw InvalidSyntaxParseError("Invalid syntax. Cannot find identifier: " + arg);
	}
}

void ParseExpr(const std::string& text, Tokens& tokens) {
	std::string func_name;
	tokens.clear();

	std::string::size_type pos = text.find('('), end_arg(text.find(')'));
	if (pos != std::string::npos && end_arg != std::string::npos) {
		tokens.add(Token(TokenType::KEYWORD, std::string(text, 0, pos)));
		std::string::size_type start_arg = ++pos;

		std::string args(text, start_arg, end_arg-start_arg);
		pos = args.find(',');
		start_arg = 0;
		tokens.add(Token(TokenType::DELIM));
		while (pos != std::string::npos) {
			ParseArg(std::string(args, start_arg, pos-start_arg), tokens);
			start_arg = ++pos;
			pos = args.find(',', start_arg);
		}
		pos = args.find_last_of(',');
		if (pos != std::string::npos) {
			ParseArg(std::string(args, ++pos), tokens);
		} else if (!args.empty()) {
			ParseArg(args, tokens);
		} else {
			throw InvalidSyntaxParseError("Invalid syntax. Cannot find parameters");
		}

	} else {
		throw InvalidSyntaxParseError("Invalid syntax. Cannot find (): " + text);
	}
}

FunctionSpec SyntAnalizer(Tokens& token)
{
	FunctionSpec result;

	if (token.accept(TokenType::KEYWORD, result.name) && token.expect(TokenType::DELIM)) {
		while (!token.empty()) {
			std::string arg;

			if(token.accept(TokenType::IDENTIFIER, arg)) {
				std::string value("NO_DEFAULT");
				if (token.expect(TokenType::DELIM)) {
					;
				} else if (token.expect(TokenType::OP) && token.accept(TokenType::NUMBER, value)) {
					if (token.expect(TokenType::DELIM)) {
						;
					} else {
						throw InvalidSyntaxParseError("Invalid expression. Expected delimiter: " + arg);
					}
				} else {
					throw InvalidSyntaxParseError("Invalid expression. " + arg);
				}

				result.params.emplace_back(SpecParams::value_type(arg, value));

				if (token.size() == 1) {
					if (token.expect(TokenType::DELIM)) {
						;
					} else {
						throw InvalidSyntaxParseError("Invalid expression. Expected delimiter: " + arg);
					}
				}
			}
		}
	} else {
		throw InvalidSyntaxParseError("Invalid syntax. Expected format: keyword(");
	}

	return result;
}


LexParser::LexParser() {

}

LexParser::~LexParser() {

}

FunctionSpec LexParser::parseSpec(const std::string &f) {
    FunctionSpec result;
	Tokens tokens;
	ParseExpr(f, tokens);
	result = SyntAnalizer(tokens);
	return result;
}

} // namespace Eq
