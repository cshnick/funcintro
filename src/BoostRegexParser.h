#pragma once

#include "Common.h"


namespace Eq {

class BoostRegexParser final : public Parser {
public:
	BoostRegexParser();
	~BoostRegexParser() {}

	FunctionSpec parseSpec(const std::string &f) override;
};

} // namespace Eq
