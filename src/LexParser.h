#pragma once

#include "Common.h"

namespace Eq {

class LexParser final : public Parser {
public:
	LexParser();
	virtual ~LexParser();

	FunctionSpec parseSpec(const std::string &f) override;
};

} // namespace Eq
