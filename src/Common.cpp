#include "Common.h"

#include <functional>
#include <algorithm>

#include "ParserFactory.h"


namespace Eq {

namespace Private {

class SpecMapPrivate {
public:
	SpecMapPrivate(SpecMap *q) :
			q(q) {
		parser_.reset(ParserFactory::inst().create(Name::LexParser));
	}
	~SpecMapPrivate() {
	}

	const SpecMap::ValueType &all() const {
		return data_;
	}
	void reg(const std::string &fstr) {
		FunctionSpec s { parser_->parseSpec(fstr) };
		if (data_.find(s.name) != data_.end()) {
			throw SpecExistingError("The spec '" + s.name + "' is registered");
		}
		data_.emplace(s.name, s);
	}
	const FunctionSpec &get(const std::string &key) const {
		try {
			return data_.at(key);
		} catch (std::out_of_range &e) {
			throw NotRegisteredSpecError("Key '" + key + "' is not registered");
		}
	}
private:
	SpecMap *q;

	std::unique_ptr<Parser> parser_;
	SpecMap::ValueType data_;
};

} // namespace Private

SpecMap &SpecMap::inst() {
	static SpecMap i;
	return i;
}

SpecMap::SpecMap() :
		d(new Private::SpecMapPrivate(this)) {
}

SpecMap::~SpecMap() {

}

const SpecMap::ValueType &SpecMap::all() const {
	return d->all();
}

void SpecMap::reg(const std::string &stringspec) {
	d->reg(stringspec);
}

const FunctionSpec &SpecMap::get (const std::string &key) const {
	return d->get(key);
}


namespace Private {

class FuncProcessorPrivate {
public:
	using CallFunction = std::function<double(FunctionSpec)>;
	using CallMap = std::map<std::string, CallFunction>;

	FuncProcessorPrivate(FuncProcessor *q) :
			q(q) {
		parser_.reset(ParserFactory::inst().create(Name::LexParser));
		///Using std::function as it can be performed different ways either lamda:
		sCallMap.emplace("linear", [](const FunctionSpec& spec) {
			std::cout << "Hello from 'linear' call" << std::endl;
			std::cout << spec << std::endl;

			return 0.0;
		});
		//Or member function
		using namespace std::placeholders;
		sCallMap.emplace("bicubic", std::bind(&FuncProcessorPrivate::bicubic, this, _1));
	}
	double invokeDefault (const FunctionSpec &sp) {
		double result = 0.0;
		auto iter = sCallMap.find(sp.name);
		if (iter != sCallMap.end()) {
			CallFunction callf = iter->second;
			result = callf(sp);
		} else {
			result = stub(sp);
		}
		return result;
	}
	double process(const std::string &func) {
		FunctionSpec resultSpec;
		mergeSpecs(func, resultSpec);
		return invokeDefault(resultSpec);
	}
	double bicubic(const FunctionSpec &spec) {
		std::cout << "Hello from 'bicubic' call" << std::endl;
		std::cout << spec << std::endl;

		return 0.0;
	}
	double stub(const FunctionSpec &spec) {
		std::cout << "Default function call for spec:" << std::endl;
		std::cout << spec << std::endl;
		return 0.0;
	}
	void mergeSpecs(const std::string &func, FunctionSpec &destspec) {
		FunctionSpec callspec = parser_->parseSpec(func);
		FunctionSpec funcspec = SpecMap::inst().get(callspec.name);
		destspec = funcspec;
		for (size_t i = 0; i < destspec.params.size(); i++) {
			SpecParam &funcparam = destspec.params.at(i);
			///Positional arguments
			if (i < callspec.params.size() && callspec.params.at(i).second == "NO_DEFAULT") {
				if (i >= callspec.params.size()) {
					throw SpecMismatchParseError("Positional arguments mismatch");
				}
				funcparam.second = callspec.params.at(i).first;
			///Named arguments
			} else {
				auto it = std::find_if(
						callspec.params.begin(),
						callspec.params.end(),
						[&funcparam] (SpecParam &p) {
					return funcparam.first == p.first;
				});
				if (it != callspec.params.end()) {
					funcparam.second = (*it).second;
				} else if (funcparam.second == "NO_DEFAULT") {
					throw SpecMismatchParseError("Invalid parameter " + funcparam.first);
				} 
			}
		}
	}
	double calculateSpec(FunctionSpec &srcSpec) {
		return 0.0;
	}
private:
	static CallMap sCallMap;
	std::unique_ptr<Parser> parser_;
	FuncProcessor *q;
};
FuncProcessorPrivate::CallMap FuncProcessorPrivate::sCallMap;

} // namespace Private


FuncProcessor::FuncProcessor() {
	d.reset(new Private::FuncProcessorPrivate(this));
}

FuncProcessor::~FuncProcessor() {

}

double FuncProcessor::process(const std::string &func) {
	return d->process(func);
}

std::ostream &operator<<(std::ostream &ostr, const SpecParam &sp) {
	ostr << "{" << R"( "key": ")" << sp.first << R"(", "val": ")" << sp.second
			<< R"("})";
	return ostr;
}

std::ostream &operator<<(std::ostream &ostr, const SpecParams &sps) {
	ostr << "[";
	for (const SpecParam &sp : sps) {
		ostr << sp;
		if (&sp != &sps.back())
			ostr << ",";
	}
	ostr << "]";
	return ostr;
}

std::ostream &operator<<(std::ostream &ostr, const FunctionSpec &fs) {
	ostr << "{" << R"( "name": ")" << fs.name << R"(", "params": )" << fs.params
			<< R"(})";
	return ostr;
}

std::ostream &operator<<(std::ostream &ostr, const SpecMap &sm) {
	const SpecMap::ValueType &smref(sm.all());
	ostr << "[";
	for (SpecMap::ValueType::const_iterator it = smref.begin();
			it != smref.end();) {
		ostr << "{" << R"( "funcname": ")" << it->first
				<< R"(", "functionspec": )" << it->second << R"(})";
		if (++it != smref.end())
			ostr << ",";
	}
	ostr << "]";
	return ostr;
}

} // namespace Eq
