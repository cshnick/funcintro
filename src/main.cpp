#include <iostream>
#include <fstream>
#include <exception>
#include <functional>
#include "Common.h"
#include "ParserFactory.h"


using namespace Eq;

std::string usage(const std::string &argv0) {
	std::string basename = argv0.substr(argv0.find_last_of("/\\") + 1);
	return "Usage: " + basename + " <filefunctionspec> <filefunctioncall>";
}

void checkArgs(int argc, char **argv) {
	if (argc < 3) {
		throw InputDataError("too few arguments");
	}
}

using ReportFunc = std::function<void(std::string, std::exception&)>;
void read_and(const std::string &filename, std::function<void(std::string, ReportFunc)> callable) {
	std::string linebuffer;
	std::ifstream specstream(filename, std::fstream::in);
	if (!specstream.is_open()) {
		throw InputDataError("can't open '" + filename + "' for reading");
	}
	int cnt = 0;
	while (std::getline(specstream, linebuffer)) {
		if (linebuffer.at(0) != '#') {
			ReportFunc report = [filename, cnt] (const std::string &s, std::exception &e) {
				std::cerr << s << " in '" << filename + ":" << cnt << "': " << e.what() << std::endl;
			};
			callable(linebuffer, report);
			cnt++;
		}
	}
}

int main (int argc, char **argv) {
	try {
		checkArgs(argc, argv);

		read_and(argv[1], [] (const std::string &specline, ReportFunc report) {
			try {
				SpecMap::inst().reg(specline);
				std::cout << "=== Successfully registered " << specline << std::endl;

			} catch  (BaseParseError &e) {
				report("skipping parse error", e);

			} catch (SpecExistingError &e) {
				report("skipping error", e);
			}
		});

		std::cout << "Spec map: " << SpecMap::inst() << std::endl;

		FuncProcessor c;
		read_and(argv[2], [&c] (const std::string &specline, ReportFunc report) {
			try {
				c.process(specline);
				std::cout << "*** Successfully processed " << specline << std::endl;

			} catch (BaseParseError &e) {
				report("skiping process error", e);

			} catch (NotRegisteredSpecError &e) {
				report("skiping process error", e);
			}
		});

	} catch (InputDataError &e) {
		std::cerr << "Err: " << e.what() << std::endl;
		std::cout << usage(argv[0]) << std::endl;
		return 0;

	} catch (std::exception &e) {
		std::cerr << "Err: " << e.what() << std::endl;
		return -1;
	}

    return 0;
}
