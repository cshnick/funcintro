#include "BoostRegexParser.h"

#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>

namespace Eq {

BoostRegexParser::BoostRegexParser() {}

namespace {
void splitNameArgs(const std::string source, std::string &destname, std::string &destargs) {
	boost::regex regx("(.*)\\((.*)\\)");
	boost::smatch resx;
	boost::regex_match(source, resx, regx);

	switch (resx.size()) {
	case 3:
		/// Expected result
		break;
	case 2:
		throw ArgumentsStringParseError("Error at parsing function name or argument list");
	default:
		throw WholeStringParseError("Error at string parsing");
	}

	destname = resx[1];
	destargs = resx[2];

	boost::trim(destname);
	boost::trim(destargs);
}

void parseSpecParams(const std::string &source, SpecParams &destparams) {
	std::vector<std::string> argsvec;
	boost::split(argsvec, source, boost::is_any_of(","), boost::token_compress_on);
	for (const std::string &arg: argsvec) {
		std::string targ = boost::trim_copy(arg);
		std::vector<std::string> defvec;
		boost::split(defvec, targ, boost::is_any_of("="), boost::token_compress_on);
		destparams.push_back(std::make_pair(defvec.at(0), defvec.size() > 1 ? defvec.at(1) : "NO_DEFAULT"));
	}
}

} // namespace

FunctionSpec BoostRegexParser::parseSpec(const std::string &fstr) {
	FunctionSpec result;

	std::string namestr, argsstr;
	splitNameArgs(fstr, result.name, argsstr);
	parseSpecParams(argsstr, result.params);

	return result;
}

} // namespace Eq
