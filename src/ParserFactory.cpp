#include "ParserFactory.h"
#include "BoostRegexParser.h"
#include "LexParser.h"


namespace Eq {

namespace Name {

std::string BoostRegexParser("boostregexparser");
std::string LexParser("lexparser");

} //namespace Eq


ParserFactory::ParserFactory() {
	reg<BoostRegexParser>(Name::BoostRegexParser);
	reg<LexParser>(Name::LexParser);
}

ParserFactory &ParserFactory::inst() {
	static ParserFactory i;
	return i;
}

} //namespace Name
